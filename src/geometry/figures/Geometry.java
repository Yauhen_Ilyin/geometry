package geometry.figures;

public abstract class Geometry {
    
    private String name;
    private double centerX;
    private double centerY;
    
    public Geometry(String name, double centerX, double centerY) {
        this.name = name;
        this.centerX = centerX;
        this.centerY = centerY;
    }
    
    public double getcenterX(){
        return this.centerX;
    }
    
    public double getcenterY(){
        return this.centerY;
    }

}
package geometry.figures;

public class Line extends Geometry {

	private double startX;
	private double startY;
	private double endX;
	private double endY;

	public Line(String name, Point start, Point end) {
		super(name, (start.getcenterX() + end.getcenterX()) / 2, (start.getcenterY() + end.getcenterY()) / 2);
		this.startX = start.getcenterX();
		this.startY = start.getcenterY();
		this.endX = end.getcenterX();
		this.endY = end.getcenterY();
	}

	public double getStartX() {
		return startX;
	}

	public double getStartY() {
		return startY;
	}

	public double getEndX() {
		return endX;
	}

	public double getEndY() {
		return endY;
	}

}